/**
 * Created by emrekilic on 1/26/16.
 */


var imageArea = document.getElementById('imageArea');
var image = document.querySelector('#imageArea > img');

imageArea.style.cssText = "position:absolute;right:0px;left:0px;top:0;bottom:0;z-index:-1";
image.style.cssText = "display:block;position:relative;right:0px;left:0px;top:0;bottom:0;width:100%;height:100%";